[![pipeline status](https://gitlab.com/carletonblueprint/beneficent/badges/master/pipeline.svg)](https://gitlab.com/carletonblueprint/beneficent/commits/master)

# Getting Started

### Windows Users Only
```
0) Install Docker Desktop using the instructions below

Follow steps 1-5 here: https://docs.microsoft.com/en-us/windows/wsl/install-win10

Install Docker Desktop: https://docs.docker.com/docker-for-windows/wsl/

1) Install Notepad++: https://notepad-plus-plus.org/downloads/

2) Clone the project on to your local machine:

$ git clone https://gitlab.com/carletonblueprint/beneficent.git

3) Navigate to the "beneficent/services/users" folder in your File Explorer

4) Open "entrypoint.sh" using Notepad++

5) Go to Edit -> EOL conversion -> change from CRLF to LF

6) Save and exit the file

After that continue from STEP 2 below

```

## Initial Setup
```
0) Install Docker Desktop

1) Clone the project on to your local machine:

$ git clone https://gitlab.com/carletonblueprint/beneficent.git

2) Run the following commands:

$ cd beneficent
$ export REACT_APP_USERS_SERVICE_URL=http://localhost:8007

3) Build and start the Docker images:
$ docker-compose build
$ docker-compose up -d

4) Create and seed the database:
$ docker-compose exec users python manage.py recreate_db 
$ docker-compose exec users python manage.py seed_db
```
You should now be able to ping: http://localhost:5001/ping    
You should be able to view the Swagger API doc at: http://localhost:5001/doc/    
And you should be able view the React app at: http://localhost:3007

To run tests and the linter please use the Docker commands explained below.

## Docker Workflow
 Environment Variable
```
$ export REACT_APP_USERS_SERVICE_URL=http://localhost:5001
```

Build the images:
```
$ docker-compose build
```
Run the containers:
```
$ docker-compose up -d
```
Create the database:
```
$ docker-compose exec users python manage.py recreate_db
```
Seed the database:
```
$ docker-compose exec users python manage.py seed_db
```
Run the tests:
```
$ docker-compose exec users python -m pytest "project/tests" -p no:warnings
```
Run the tests with coverage:
```
$ docker-compose exec users python -m pytest "project/tests" -p no:warnings --cov="project"
```
Lint the Python code:
```
$ docker-compose exec users flake8 project
```
Run Black and isort with check options:
```
$ docker-compose exec users black project --check
$ docker-compose exec users /bin/sh -c "isort project/**/*.py --check-only"
```
Make code changes with Black and isort:
```
$ docker-compose exec users black project
$ docker-compose exec users /bin/sh -c "isort project/**/*.py"
```
Run the client-side tests:
```
$ docker-compose exec client npm test
```
Run the client-side tests with coverage:
```
$ docker-compose exec client react-scripts test --coverage
```
Lint the JavaScript:
```
$ docker-compose exec client npm run lint
```
Run Prettier:
```
$ docker-compose exec client npm run prettier:check
$ docker-compose exec client npm run prettier:write
```

## Other Commands
To stop the containers:
```
$ docker-compose stop
```
To bring down the containers:
```
$ docker-compose down
```
## Postgres

Want to access the database via psql?
```
$ docker-compose exec users-db psql -U postgres
```
Then, you can connect to the database and run SQL queries. For example:
```
# \c users_dev
# select * from users;
```

## Project Structure
```
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile.deploy
├── README.md
├── docker-compose.yml
├── release.sh
└── services
    ├── client
    │   ├── .dockerignore
    │   ├── .eslintrc.json
    │   ├── .gitignore
    │   ├── Dockerfile
    │   ├── Dockerfile.ci
    │   ├── README.md
    │   ├── coverage
    │   ├── package-lock.json
    │   ├── package.json
    │   ├── public
    │   │   ├── favicon.ico
    │   │   ├── index.html
    │   │   ├── logo192.png
    │   │   ├── logo512.png
    │   │   ├── manifest.json
    │   │   └── robots.txt
    │   └── src
    │       ├── App.jsx
    │       ├── components
    │       │   ├── About.jsx
    │       │   ├── AddUser.jsx
    │       │   ├── LoginForm.jsx
    │       │   ├── Message.jsx
    │       │   ├── NavBar.css
    │       │   ├── NavBar.jsx
    │       │   ├── RegisterForm.jsx
    │       │   ├── UserStatus.jsx
    │       │   ├── UsersList.jsx
    │       │   ├── __tests__
    │       │   │   ├── About.test.jsx
    │       │   │   ├── AddUser.test.jsx
    │       │   │   ├── App.test.jsx
    │       │   │   ├── LoginForm.test.jsx
    │       │   │   ├── Message.test.jsx
    │       │   │   ├── NavBar.test.jsx
    │       │   │   ├── RegisterForm.test.jsx
    │       │   │   ├── UserStatus.test.jsx
    │       │   │   ├── UsersList.test.jsx
    │       │   │   └── __snapshots__
    │       │   │       ├── About.test.jsx.snap
    │       │   │       ├── AddUser.test.jsx.snap
    │       │   │       ├── App.test.jsx.snap
    │       │   │       ├── LoginForm.test.jsx.snap
    │       │   │       ├── Message.test.jsx.snap
    │       │   │       ├── NavBar.test.jsx.snap
    │       │   │       ├── RegisterForm.test.jsx.snap
    │       │   │       ├── UserStatus.test.jsx.snap
    │       │   │       └── UsersList.test.jsx.snap
    │       │   └── form.css
    │       ├── index.js
    │       └── setupTests.js
    ├── nginx
    │   └── default.conf
    └── users
        ├── .coverage
        ├── .coveragerc
        ├── .dockerignore
        ├── Dockerfile
        ├── Dockerfile.prod
        ├── entrypoint.sh
        ├── htmlcov
        ├── manage.py
        ├── project
        │   ├── __init__.py
        │   ├── api
        │   │   ├── __init__.py
        │   │   ├── auth.py
        │   │   ├── ping.py
        │   │   └── users
        │   │       ├── __init__.py
        │   │       ├── admin.py
        │   │       ├── crud.py
        │   │       ├── models.py
        │   │       └── views.py
        │   ├── config.py
        │   ├── db
        │   │   ├── Dockerfile
        │   │   └── create.sql
        │   └── tests
        │       ├── __init__.py
        │       ├── conftest.py
        │       ├── pytest.ini
        │       ├── test_admin.py
        │       ├── test_auth.py
        │       ├── test_config.py
        │       ├── test_ping.py
        │       ├── test_user_model.py
        │       ├── test_users.py
        │       └── test_users_unit.py
        ├── requirements-dev.txt
        ├── requirements.txt
        └── setup.cfg
```
