import { useLayoutEffect, useState } from 'react';

/**
 * This hook will return the size of the browser window
 * @returns windowSize === [width, height]
 * 
 * **Reference:** A Stackoverflow Post
 *  - Author: Sophie Alpert
 *  - Editor: Matt Fletcher
 *  - Link: https://stackoverflow.com/a/19014495/13113295
 */
function useWindowSize() {
  const [size, setSize] = useState([0, 0]);

  /**
   * This hook will add an event listener to the window size
   * and reset the size state when ever the size changes.
   */
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  return size;
}

export default useWindowSize;