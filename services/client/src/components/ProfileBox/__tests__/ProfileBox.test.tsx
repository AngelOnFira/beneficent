import { cleanup, fireEvent, render, wait } from '@testing-library/react';
import { AxiosResponse } from 'axios';
import * as React from 'react';
import ProfileBox from '../ProfileBox';

afterEach(cleanup);

const emptyInit = {
  firstName: "",
  lastName: "",
  address: "",
  phoneNum: "",
  province: ""
}

const filledInit = {
  firstName: "Tom",
  lastName: "Zhu",
  address: "123 Sesame Street",
  phoneNum: "1-800-267-2001",
  province: "Ontario"
}

const otherProps = {
  handleUpdateProfile: () => new Promise<AxiosResponse>(resolve => resolve())
}

it("matches snapshot", () => {
  const { asFragment } = render(<ProfileBox initialValues={emptyInit} {...otherProps}/>);
  expect(asFragment()).toMatchSnapshot();
});

it("renders properly", () => {
  const { getByPlaceholderText } = render(<ProfileBox initialValues={filledInit} {...otherProps}/>);
  
  const firstNameInput: any = getByPlaceholderText("First Name");
  const lastNameInput: any = getByPlaceholderText("Last Name");
  const phoneNumInput: any = getByPlaceholderText("Phone Number");
  const addressInput: any = getByPlaceholderText("Address")

  expect(firstNameInput.value).toBe(filledInit.firstName);
  
  expect(lastNameInput.value).toBe(filledInit.lastName);

  expect(addressInput.value).toBe(filledInit.address);

  expect(phoneNumInput.value).toBe(filledInit.phoneNum);

});

describe("handles validation correctly", () => {
  it("handles valid inputs", async () => {
    const { getByPlaceholderText } = render(<ProfileBox initialValues={emptyInit} {...otherProps}/>);
    
    const firstNameInput = getByPlaceholderText("First Name");
    const phoneNumInput = getByPlaceholderText("Phone Number");
    const lastNameInput = getByPlaceholderText("Last Name");
    const addressInput = getByPlaceholderText("Address");

    await wait(() => {
      fireEvent.change(firstNameInput, { target: { value: "John" } });
      fireEvent.blur(firstNameInput);

      fireEvent.change(phoneNumInput, { target: { value: "+1 (613) 762-3313" } });
      fireEvent.blur(phoneNumInput);

      fireEvent.change(lastNameInput, { target: { value: "Doe" } }); 
      fireEvent.blur(lastNameInput);

      fireEvent.change(addressInput, { target: { value: "Street" } });
      fireEvent.blur(addressInput);

    });

    expect(firstNameInput).toHaveAttribute("aria-invalid", "false");
    expect(lastNameInput).toHaveAttribute("aria-invalid", "false");
    expect(addressInput).toHaveAttribute("aria-invalid", "false");
    expect(phoneNumInput).toHaveAttribute("aria-invalid", "false");
  });

  it("handles invalid inputs", async () => {
    const { getByPlaceholderText } = render(<ProfileBox initialValues={emptyInit} {...otherProps}/>);
    
    const firstNameInput = getByPlaceholderText("First Name");
    const phoneNumInput = getByPlaceholderText("Phone Number");
    const lastNameInput = getByPlaceholderText("Last Name");
    const addressInput = getByPlaceholderText("Address")

    await wait(() => {
      fireEvent.change(firstNameInput, { target: { value: "" } });
      fireEvent.blur(firstNameInput);

      fireEvent.change(phoneNumInput, { target: { value: "abcdefg" } });
      fireEvent.blur(phoneNumInput);

      fireEvent.change(lastNameInput, { target: { value: "" } }); 
      fireEvent.blur(lastNameInput);

      fireEvent.change(addressInput, { target: { value: "" } });
      fireEvent.blur(addressInput);
    });

    expect(firstNameInput).toHaveAttribute("aria-invalid", "true");
    expect(lastNameInput).toHaveAttribute("aria-invalid", "true");
    expect(addressInput).toHaveAttribute("aria-invalid", "true");
    expect(phoneNumInput).toHaveAttribute("aria-invalid", "true");
  });
});

it("removes disabled on click edit button", async () => {
  const { getByText } = render(<ProfileBox {...otherProps} initialValues={filledInit}/>);

  const editBtn = getByText("Edit");

  await wait(() => {
    fireEvent.click(editBtn);
  });

  expect(editBtn.firstChild?.textContent).toBe("Save");
});
