import { Avatar, Card, CardContent, CardHeader, FilledInput, InputAdornment, MenuItem, Select, Button, createMuiTheme, ThemeProvider, CircularProgress } from '@material-ui/core';
import { AccountCircle, Map, Phone, Room } from '@material-ui/icons';
import { Formik } from 'formik';
import * as Yup from 'yup';
import * as React from 'react';
import './ProfileBox.scss';
import { AxiosResponse } from 'axios';

/**
 * Theme to remove underline and add padding to inputs,
 * include custom primary color to buttons
 */
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#2189EA",
    },
    
  },
  overrides: {
    MuiFilledInput: {
      input: {
        padding: '20px 0', 
      }    
    },
    MuiOutlinedInput: {
      input: {
        padding: '20px 0', 
      }    
    },
    MuiButton: {
      outlined: {
        color: "#2189EA",
      }
    },
    MuiCard: {
      root: {
        padding: '12px'
      }
    }
  }
});

/**
 * Province and Territory list for select
 */
const provinces = ["Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Northwest Territories", "Nova Scotia", "Nunavut", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan", "Yukon"]

/**
 * Profile Box Form values type
 */
type ProfileBoxValues = {
  firstName: string,
  lastName: string,
  address: string,
  phoneNum: string,
  province: string,
}

/**
 * Props for profile box
 */
type ProfileBoxProps = {
  /**
   * Initial values for fields
   */
  initialValues: ProfileBoxValues,
  /**
   * Avatar url/path
   */
  avatar?: string,
  /**
   * Function to handle profile save, returns promise
   */
  handleUpdateProfile: (values: ProfileBoxValues) => Promise<AxiosResponse>,
}

/**
 * Profile Box Component: Form to update profile fields
 * @param param0 ProfileBoxProps
 */
const ProfileBox: React.FC<ProfileBoxProps> = ({initialValues, avatar, handleUpdateProfile}) => {
  const [editing, setEditing] = React.useState(false);

  return (
    // apply custom theme
    <ThemeProvider theme={theme}>
      <Card>
        <CardHeader 
          avatar={
            <Avatar 
              src={avatar}
              alt="Your Avatar">
                {avatar && "NA"}
            </Avatar>
          }
          titleTypographyProps={{
            variant: "h6",
            style: {
              fontWeight: 700
            }
          }}
          title="Profile"
          subheader="Edit your profile details here"
        />
        <CardContent>
          <Formik
            onSubmit={(values, { setSubmitting }) => {
              // TODO: Make request to DB to update fields, PUT
              handleUpdateProfile(values).then(res => {
                setSubmitting(false);
                setEditing(false);
              })
            }}
            enableReinitialize
            initialValues={initialValues}
            validationSchema={
              Yup.object().shape({
                firstName: Yup.string().required("First name is required"),
                lastName: Yup.string().required("Last name is required"),
                address: Yup.string().required("Address is required"),
                phoneNum: Yup.string()
                  .matches(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/g, "Please enter a valid phone number")
                  .required("Phone number is required"),
                location: Yup.string().oneOf(provinces, "Choose a valid province")
              })
            }
          >
            {(props) => {
              const { 
                values,
                touched,
                errors,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
                submitForm
              } = props;

              return (
                <form onSubmit={handleSubmit}>
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <AccountCircle/>
                      </InputAdornment>
                    }
                    name="firstName"
                    value={values.firstName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.firstName && errors.firstName)}
                    fullWidth
                    placeholder="First Name"
                    disableUnderline={!(touched.firstName && errors.firstName)}
                    disabled={!editing}
                  />
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <AccountCircle/>
                      </InputAdornment>
                    }
                    name="lastName"
                    value={values.lastName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.lastName && errors.lastName)}
                    fullWidth
                    placeholder="Last Name"
                    disableUnderline={!(touched.lastName && errors.lastName)}
                    disabled={!editing}
                  />
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <Room/>
                      </InputAdornment>
                    }
                    name="address"
                    value={values.address}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.address && errors.address)}
                    fullWidth
                    placeholder="Address"
                    disableUnderline={!(touched.address && errors.address)}
                    disabled={!editing}
                  />
                  <FilledInput
                    startAdornment={
                      <InputAdornment position="start">
                        <Phone/>
                      </InputAdornment>
                    }
                    name="phoneNum"
                    value={values.phoneNum}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.phoneNum && errors.phoneNum)}
                    fullWidth
                    placeholder="Phone Number"
                    disableUnderline={!(touched.phoneNum && errors.phoneNum)}
                    disabled={!editing}
                  />
                  <Select 
                    style={{
                      width: '50%'
                    }}
                    data-testid="select"
                    value={values.province}
                    variant="outlined"
                    name="province"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={!!(touched.province && errors.province)}
                    disabled={!editing}
                    startAdornment={
                      <InputAdornment position="start">
                        <Map/>
                      </InputAdornment>
                    }
                  >
                    {provinces.map(name => {
                      return <MenuItem value={name} key={name}>{name}</MenuItem>
                    })}
                  </Select>
                  <Button // change button depending on if currently editing
                    variant={editing ? "contained":"outlined"}
                    onClick={() => editing ? submitForm() : setEditing(true)} 
                    color={editing ? "primary":"default"}
                    style={{
                      boxShadow: "none",
                      float: 'right',
                      margin: '16px 32px'
                    }}
                  >
                    {isSubmitting ? <CircularProgress size={22}/> : (editing ? "Save":"Edit")}
                  </Button>
                </form>
              );
            }}
          </Formik>
        </CardContent>
      </Card>
    </ThemeProvider>
  );
}

export default ProfileBox;