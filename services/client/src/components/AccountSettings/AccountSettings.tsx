import React from 'react';
import {
  withStyles,
} from '@material-ui/core/styles';

import {
  Grid,
  Typography,
  Paper 
} from '@material-ui/core';

import './AccountSettings.scss';
import ProfileBox from '../ProfileBox/ProfileBox';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { useHistory } from 'react-router-dom';
import routes from '../../routes';

/**
 * A styled typography component for the titles
 */
const TitleTypography = withStyles({
  h4: {
    paddingLeft: 15,
    fontWeight: 700
  }
})(Typography);

type AccountSettingsProps = {
  /**
   * Function to refresh tokens, returns true if successful
   */
  validRefresh: () => boolean,

  /**
   * User's access token for authorization
   */
  accessToken: string,
}

/**
 * AccountSettings Component to display profile, preferences, contact details
 * @param props AccountSettingsProps
 */
const AccountSettings: React.FC<AccountSettingsProps> = (props) => {
  const {validRefresh, accessToken} = props;
  const [initValues, setInitValues] = React.useState({
    firstName: "",
    lastName: "",
    address: "",
    phoneNum: "",
    province: ""
  });

  const history = useHistory();

  // retrieve user data using access token
  React.useEffect(() => {
    if (accessToken) { // if access token is valid, get data
      const options: AxiosRequestConfig = {
        // TODO: Change this url to the GET for user profile data, get avatar
        url: `${process.env.REACT_APP_USERS_SERVICE_URL}/auth/status`,
        method: "get",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${accessToken}`
        }
      }
      axios(options).then(res => {
        setInitValues({
          firstName: "Tom",
          lastName: "Zhu",
          address: "123 Sesame Street",
          phoneNum: "1-800-267-2001",
          province: "Ontario"
        });
      }).catch(err => {
        console.log(err);
      });
    } else {
      if (!validRefresh()) {
        history.push(routes.login.path);
      }
    }
  }, [accessToken]);

  const handleUpdateProfile = (values) => {
    return new Promise<AxiosResponse>(resolve => {
      setTimeout(() => {
        resolve(axios.get(`${process.env.REACT_APP_USERS_SERVICE_URL}/ping`));
      }, 1000);
    });
  }

  return (
    <Grid container direction="column">
      <Grid item>
        <TitleTypography
          variant="h4"
          color="textPrimary"
        >
          Account Settings
        </TitleTypography>
      </Grid>
      <Grid
        spacing={3}
        container
        item
        direction="row"
        className="account-settings-body"
      >
        <Grid
          item
          container
          alignItems="center"
          direction="column"
          lg={8}
          spacing={3}
        >
          <Grid
            item
            className="box"
          >
            <ProfileBox initialValues={initValues} handleUpdateProfile={handleUpdateProfile}/>
          </Grid>
          <Grid
            item
            className="box"
          >
            {/* TODO Replace the following Paper by the <PreferencesBox /> component */}
            <Paper style={{ width: '100%', height: 400 }}>
              Preferences Box
            </Paper>
          </Grid>
        </Grid>
        <Grid
          item
          className="box"
          lg
        >
          {/* TODO Replace the following Paper by the <NotificationsBox /> component */}
          <Paper style={{ width: '100%', height: 250 }}>
            Notification Box
          </Paper>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default AccountSettings;