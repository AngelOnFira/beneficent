import * as React from 'react';
import { cleanup, render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import AccountSettings from '../AccountSettings';

afterEach(cleanup);

const props = {
  accessToken: "token",
  validRefresh: () => true,
}

it('render correctly', () => {
  const { getByText } = render(
    <BrowserRouter>
      <AccountSettings {...props} />
    </BrowserRouter>
  );
  expect(getByText('Account Settings')).toHaveClass('MuiTypography-root');
  expect(getByText('Account Settings')).toHaveClass('MuiTypography-h4');
});

it('rendering matches snapshot', () => {
  const { asFragment } = render(
    <BrowserRouter>
      <AccountSettings {...props}/>
    </BrowserRouter>
  );
  expect(asFragment()).toMatchSnapshot();
});


