import { Box, Grid } from "@material-ui/core";
import * as React from "react";
import "./FormCardLayout.scss";
import logo from "../../assets/beneficient-logo.png";

type FormCardLayoutProps = {
  /**
   * The specific form component
   * Example: <LoginForm />, <PasswordResetForm />, etc.
   */
  children: React.ReactNode;
};

/**
 * The Box layout of form pages.
 * Passing a specific form component to get the according form page.
 */
const FormCardLayout = ({ children }: FormCardLayoutProps) => {
  return (
    <Grid
      className="container"
      container
      direction="column"
      alignItems="center"
      justify="center"
    >
      <Box className="form-box" borderRadius={15}>
        <div className="beneficent-panel">
          <img
            className="beneficent-logo"
            src={logo}
            alt="Beneficent Logo"
          />
        </div>

        <div
          data-testid="form-panel"
          className="form-panel"
        >
          {children}
        </div>
      </Box>
    </Grid>
  );
};

export default FormCardLayout;
