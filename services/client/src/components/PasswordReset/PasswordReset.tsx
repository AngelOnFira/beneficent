import { Button, Container, createMuiTheme, createStyles, IconButton, InputAdornment, makeStyles, TextField, TextFieldProps, ThemeProvider, Typography } from "@material-ui/core";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import axios, { AxiosRequestConfig } from "axios";
import { Formik } from "formik";
import * as React from "react";
import { useHistory, useParams } from "react-router-dom";
import * as Yup from "yup";
import FormCardLayout from "../FormCardLayout/FormCardLayout";
import "./PasswordReset.scss";
import checkmark from "../../assets/blue_check_24px.png";
import ROUTES from "../../routes";

/**
 * Custom MUI Theme to add custom blue colour
 */
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#2f5BE3",
    },
  },
});

/**
 * Add success style from LoginForm
 */
const useStyles = makeStyles(() =>
  createStyles({
    success: {
      borderColor: (props: TextFieldProps) => !props.error && props.value ? '#2ED47A' : undefined,
    },
    absolute: {
        position: "absolute",
        top: "56px"
    }
  }),
);
/**
 * Password Field Component with visibility toggle
 * @param props TextFieldProps from material-ui TextField
 */
const PasswordField = (props: TextFieldProps ) => {
  const [show, setShow] = React.useState(false);

  const cls = useStyles(props);

  return (
    <TextField
      className='text-field'
      variant="outlined"
      fullWidth
      type={show ? "text" : "password"}
      InputProps={{
        classes: {
          notchedOutline: cls.success,
        } as any,
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              onClick={() => setShow(!show)}
              aria-label="Toggle Password Visibility"
            >
              {show ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        ),
      }}
      FormHelperTextProps={{
          classes: {
              root: cls.absolute
          }
      }}
      {...props}
    />
  );
};

/**
 * Success Message Props
 * @property On click event for go to login button
 */
type SuccessMessageProps = {
  redirectToLogin: (event: React.MouseEvent<HTMLButtonElement>) => void;
};


/**
 * Success message when password reset is successful
 * @param param0 - Object with redirectedToLogin property: function called when go to login button is clicked
 */
const SuccessMessage: React.FC<SuccessMessageProps> = ({ redirectToLogin }) => {
  return (
    <>
      <div className="centered">
        <img alt="check-mark" className="check" src={checkmark} />
      </div>
      <Typography className="heading" variant="h3">
        You're all set!
      </Typography>
      <p style={{ marginBottom: "40px" }} className="subtitle">
        Password reset successful. Proceed to login?
      </p>
      <Button color="primary" onClick={redirectToLogin} variant="contained">
        Login
      </Button>
    </>
  );
};

/**
 * Password Reset Form Component
 */
const PasswordResetForm = () => {
  let { token } = useParams<{ token: string }>();

  //console.log(token);

  let history = useHistory();
  const [name, setName] = React.useState("");
  const [success, setSuccess] = React.useState(false);

  /**
   * Determine if current token is valid, redirect if it is not
   */
  const getAuthStatus = () => {
    const options: AxiosRequestConfig = {
      url: `${process.env.REACT_APP_USERS_SERVICE_URL}/auth/status`,
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    };

    axios(options)
      .then(res => {
        setName(res.data.name);
      })
      .catch(error => {
        history.push(ROUTES.login.path);
      });
  };

  /**
   * Request information based on token (ie. email)
   */
  React.useEffect(() => {
    // deal with tokens, remove comment when backend implemented **************
    // getAuthStatus();
    
  }, []);

  return (
    // provide custom theme
    <ThemeProvider theme={theme}>
      <FormCardLayout>
        <Container maxWidth="xs">
          {success ? (
            <SuccessMessage redirectToLogin={() => history.push("/login")} />
          ) : (
              <>
                <Typography className="heading" variant="h3">
                  Reset Password
              </Typography>
                <p className="subtitle">
                  Hi {name}, please enter a new password below.
              </p>
                <Formik
                  initialValues={{ pass: "", confirm: "" }}
                  onSubmit={(values, { setSubmitting }) => {
                    // send new password
                    const options: AxiosRequestConfig = {
                      url: `${process.env.REACT_APP_USERS_SERVICE_URL}/auth/reset-password`,
                      method: "post",
                      headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`,
                      },
                      data: {
                        password: values.pass,
                      },
                    };

                    axios(options)
                      .then((res) => {
                        // good response
                        setSuccess(true);
                      })
                      .catch((err) => {
                        console.log(err);
                      })
                      .finally(() => {
                        setSubmitting(false);
                        setSuccess(true); // ONLY FOR TESTING -- will remove once backend added
                      });
                  }}
                  validationSchema={Yup.object().shape({
                    pass: Yup.string()
                      .min(8, "Must be at least 8 characters")
                      .required("Password is required"),
                    confirm: Yup.string()
                      .equals([Yup.ref("pass")], "Passwords do not match")
                      .required("Re-enter your password"),
                  })}
                >
                  {(props) => {
                    const {
                      values,
                      handleChange,
                      touched,
                      errors,
                      handleBlur,
                      handleSubmit,
                      isSubmitting,
                    } = props;
                    return (
                      <form onSubmit={handleSubmit}>
                        <PasswordField
                          error={!!(errors.pass && touched.pass)}
                          helperText={errors.pass && touched.pass && errors.pass}
                          onBlur={handleBlur}
                          onChange={handleChange}
                          name="pass"
                          value={values.pass}
                          disabled={isSubmitting}
                          placeholder="Password"

                        />
                        <PasswordField
                          error={!!(errors.confirm && touched.confirm)}
                          helperText={
                            errors.confirm && touched.confirm && errors.confirm
                          }
                          onBlur={handleBlur}
                          onChange={handleChange}
                          name="confirm"
                          value={values.confirm}
                          disabled={isSubmitting}
                          placeholder="Verify Password"
                        />
                        <div className="buttons">
                          <Button
                            variant="contained"
                            color="primary"
                            disabled={
                              !!(
                                !(touched.pass && touched.confirm) ||
                                errors.pass ||
                                errors.confirm
                              ) || isSubmitting
                            }
                            type="submit"
                            
                          >
                            Reset
                        </Button>
                          <Button
                            variant="outlined"
                            color="primary"
                            onClick={() => history.push("/login")}
                            disabled={isSubmitting}
                          >
                            Cancel
                        </Button>
                        </div>
                      </form>
                    );
                  }}
                </Formik>
              </>
            )}
        </Container>
      </FormCardLayout>
    </ThemeProvider>
  );
};

export default PasswordResetForm;
