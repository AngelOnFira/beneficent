import * as React from 'react';
import { cleanup, render, act, fireEvent, wait } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import PasswordResetRequest from '../PasswordResetRequest';

afterEach(cleanup);

const mockEmail = 'myemail@abc.com';

it('render correctly', () => {
  const { getByText } = render(
    <BrowserRouter>
      <PasswordResetRequest />
    </BrowserRouter>
  );
  expect(getByText('Enter your email below and we will help recover your account.')).toHaveClass('text');
});

it('handle change text field properly', () => {
  const { getByPlaceholderText } = render(
    <BrowserRouter>
      <PasswordResetRequest />
    </BrowserRouter>
  );
  const emailInput: any = getByPlaceholderText('Email');
  act(() => {
    fireEvent.change(getByPlaceholderText('Email'), {
      target: { value: mockEmail },
    })
  })

  expect(emailInput.value).toBe(mockEmail);
})

it('enter invalid email show error properly', async () => {
  const { getByPlaceholderText, getByTestId } = render(
    <BrowserRouter>
      <PasswordResetRequest />
    </BrowserRouter>
  );
  await wait(() => {
    // set email field to a text without giving correct form
    fireEvent.change(getByPlaceholderText('Email'), {
      target: { value: '#' },
    });
    fireEvent.click(getByTestId('submit-button'));
  })

  // 'aria-invalid' specifies the failed CSS class of Material UI
  expect(getByPlaceholderText('Email')).toHaveAttribute('aria-invalid', 'true');
})

it('rendering matches snapshot', () => {
  const { asFragment } = render(
    <BrowserRouter>
      <PasswordResetRequest />
    </BrowserRouter>
  );
  expect(asFragment()).toMatchSnapshot();
});
