import * as React from 'react';
import './SignUp.scss';

import Typography from '@material-ui/core/Typography';
import { TextField, TextFieldProps } from '@material-ui/core';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { Formik } from 'formik';
import * as Yup from 'yup';
import FormCardLayout from '../FormCardLayout/FormCardLayout';

interface SignUpPageProps {
  handleSubmitSignUpForm: Function
}

const PasswordField = (props: TextFieldProps) => {
  const [show, setShow] = React.useState(false);
  return (
      <TextField 
          className={`text-field`} 
          variant="outlined" 
          fullWidth 
          type={show ? "text":"password"}
          InputProps={{
              endAdornment: <InputAdornment position="end">
                  <IconButton onClick={() => setShow(!show)} aria-label="Toggle Password Visibility">
                      {show ? <Visibility/>:<VisibilityOff/>}
                  </IconButton>
              </InputAdornment>
          }} 
          {...props}/>
  );
}

const SignUp = (props: SignUpPageProps) => {
  const { handleSubmitSignUpForm } = props;

  // TODO: Add padding between title and subtitle 
  return (
    <FormCardLayout>
      <Typography className="title" variant="h3">
        Sign Up
      </Typography>

      <Typography className="text" variant="h6">
        Sign up for a new password below.
      </Typography>

      <Formik
        initialValues={{
          email: "",
          password: "",
          confirmPassword: ""
        }}
        onSubmit={async (values, { setSubmitting, resetForm, setFieldValue, setFieldError, setFieldTouched }) => {
          try {
            await handleSubmitSignUpForm(values);
          } catch (err) {
            resetForm();
            setFieldValue('email', values.email, false);
            setFieldTouched('email', true, false)
            setFieldError('email', err.message);
          }
            setSubmitting(false);
          }}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .email("Please enter a valid email.")
            .required("Email is required."),
          password: Yup.string()
            .min(8, "Must be at least 8 characters")
            .required("Password is required."),
          confirmPassword: Yup.string()
            .equals([Yup.ref('password')], "Passwords do not match")
            .required("Re-enter your password.")
        })}
      >
        {props => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;
          
          return (
            <form
              data-testid="signup-form"
              onSubmit={handleSubmit}
              className="form"
            >
              <TextField
                name="email"
                className="text-field"
                variant="outlined"
                placeholder="Email"
                value={values.email}
                onBlur={handleBlur}
                onChange={handleChange}
                error={Boolean(errors.email && touched.email)}
                helperText={errors.email}
                InputProps={{
                  style: {fontFamily: 'Lato'},
                }}
                FormHelperTextProps={{
                  style: {
                    top: 55,
                    position: 'absolute'
                  }
                }}
              />

              <PasswordField 
                error={!!(errors.password && touched.password)}
                helperText={(errors.password && touched.password && errors.password)} 
                onBlur={handleBlur} 
                onChange={handleChange} 
                name="password" 
                value={values.password} 
                disabled={isSubmitting}
                placeholder="Password" 
                FormHelperTextProps={{
                  style: {
                    top: 55,
                    position: 'absolute'
                  }
                }}
              /> 

              <PasswordField 
                error={!!(errors.confirmPassword && touched.confirmPassword)}
                helperText={(errors.confirmPassword && touched.confirmPassword && errors.confirmPassword)} 
                onBlur={handleBlur} 
                onChange={handleChange} 
                name="confirmPassword" 
                value={values.confirmPassword} 
                disabled={isSubmitting}
                placeholder="Confirm Password" 
                FormHelperTextProps={{
                  style: {
                    top: 55,
                    position: 'absolute'
                  }
                }}
              /> 

              <Button
                data-testid="signup-button"
                className="signup-button"
                color="primary"
                variant="outlined"
                type="submit"
                disabled={!!(!(touched.password && touched.confirmPassword) || 
                            (errors.password || errors.confirmPassword)) ||isSubmitting}
              >
                Register
              </Button>
            </form>
            );
          }}
        </Formik>
    </FormCardLayout>
  );
}

export default SignUp;
