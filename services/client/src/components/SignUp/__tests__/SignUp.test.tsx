import { cleanup, fireEvent, getByDisplayValue, render, wait } from '@testing-library/react';
import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';
import SignUp from '../SignUp';

afterEach(cleanup);

const mockHandleSubmitSignUpForm = async (values: Object) => {
    jest.fn()
}

it("matches snapshot", () => {
    const { asFragment } = render(<BrowserRouter><SignUp handleSubmitSignUpForm={mockHandleSubmitSignUpForm}/></BrowserRouter>);
    expect(asFragment()).toMatchSnapshot();
});

it("renders properly", () => {
    const { getByPlaceholderText, getByText } = render(<BrowserRouter><SignUp handleSubmitSignUpForm={mockHandleSubmitSignUpForm}/></BrowserRouter>);

    const passInput = getByPlaceholderText("Password");
    expect(passInput).toHaveAttribute("type", "password");
    expect(passInput).not.toHaveValue();

    const confirmInput = getByPlaceholderText("Confirm Password");
    expect(confirmInput).toHaveAttribute("type", "password");
    expect(confirmInput).not.toHaveValue();

    const submitButton = getByText("Register");
    expect(submitButton).toBeDisabled();

    expect(getByText("Sign Up")).toHaveClass("title");
});

describe("handles validation correctly", () => {
    it("handles valid inputs", async () => {
        const { getByPlaceholderText } = render(<BrowserRouter><SignUp handleSubmitSignUpForm={mockHandleSubmitSignUpForm}/></BrowserRouter>);

        const passInput = getByPlaceholderText("Password");
        const confirmInput = getByPlaceholderText("Confirm Password");
                
        await wait(() => {
            fireEvent.change(passInput, { target : { value: "something" } });
            fireEvent.blur(passInput);
            fireEvent.change(confirmInput, { target : { value: "something"}});
            fireEvent.blur(confirmInput);
        });   

        expect(passInput).toHaveAttribute("aria-invalid", 'false');
        expect(confirmInput).toHaveAttribute("aria-invalid", 'false');
    });

    it("handles passwords that don't match", async () => {
        const { getByPlaceholderText } = render(<BrowserRouter><SignUp handleSubmitSignUpForm={mockHandleSubmitSignUpForm}/></BrowserRouter>);

        const passInput = getByPlaceholderText("Password");
        const confirmInput = getByPlaceholderText("Confirm Password");
                
        await wait(() => {
            fireEvent.change(passInput, { target : { value: "s" } });
            fireEvent.blur(passInput);
            fireEvent.change(confirmInput, { target : { value: "some"}});
            fireEvent.blur(confirmInput);
        });   

        expect(passInput).toHaveAttribute("aria-invalid", 'true');
        expect(confirmInput).toHaveAttribute("aria-invalid", 'true');
    });

    it("handles too short passwords", async () => {
        const { getByPlaceholderText } = render(<BrowserRouter><SignUp handleSubmitSignUpForm={mockHandleSubmitSignUpForm}/></BrowserRouter>);

        const passInput = getByPlaceholderText("Password");
        const confirmInput = getByPlaceholderText("Confirm Password");
                
        await wait(() => {
            fireEvent.change(passInput, { target : { value: "s" } });
            fireEvent.blur(passInput);
            fireEvent.change(confirmInput, { target : { value: "s"}});
            fireEvent.blur(confirmInput);
        });   

        expect(passInput).toHaveAttribute("aria-invalid", 'true');
        expect(confirmInput).toHaveAttribute("aria-invalid", 'false');
    });
});