import * as React from "react";
import { useState } from "react";
import "./Login.scss";
import { Redirect } from "react-router-dom";
import FormCardLayout from "../FormCardLayout/FormCardLayout";
import ROUTES from "../../routes";

import {
  makeStyles,
  createStyles,
  Theme,
  MuiThemeProvider,
  createMuiTheme
} from '@material-ui/core/styles';
import {
  Typography,
  TextField,
  InputAdornment,
  Button,
  IconButton,
} from "@material-ui/core/";
import VisibilityIcon from "@material-ui/icons/Visibility";
import VisibilityOffIcon from "@material-ui/icons/VisibilityOff";
import Link from "@material-ui/core/Link";
import { Formik } from "formik";
import * as Yup from "yup";

type Account = {
  email: string,
  password: string,
}

/**
 * Custom theme for the buttons
 */
const buttonTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#2F5BE3'
    }
  },
  typography: {
    fontFamily: 'Lato',
  }
})

/**
 * Overriding styles of some inner components
 */
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    /**
     * Override the font on TextFields and their error text
     */
    inputFont: {
      fontFamily: 'Lato'
    },

    /**
     * Green color on successful validation
     */
    successfulValidation: {
      borderColor: '#2ED47A'
    }
  }),
);

interface LoginPageProps {
  /**
   * An async function that submits the login form
   * to validate email and password to server.
   */
  handleSubmitLoginForm(account: Account): Promise<void>,

  /**
   * Check if the current browser is authenticated or not.
   */
  isAuthenticated(): Boolean,
}

/**
 * Login form page component for the application.
 */
const Login = ({ handleSubmitLoginForm, isAuthenticated }: LoginPageProps) => {
  const classes = useStyles();
  /**
   * Represent the state of the "show password" button
   */
  const [showingPassword, setShowingPassword] = useState(false);

  /**
   * If the current browser is authenticated, redirect to home page
   */
  if (isAuthenticated()) {
    return <Redirect to={ROUTES.home.path} />;
  }

  return (
    <div className="login-form">
      <FormCardLayout>
        <Typography className="title" variant="h3">
          Login
        </Typography>

        <Typography className="text" variant="h6">
          Need help logging in?
          <Link href={ROUTES.contact.path}> Contact us here</Link>
        </Typography>

        <Formik
          initialValues={{
            email: "",
            password: ""
          }}
          validateOnChange={false}
          onSubmit={async (values: Account, { setSubmitting, resetForm, setFieldValue, setFieldError, setFieldTouched }) => {
            try {
              await handleSubmitLoginForm(values);
            } catch (err) {
              resetForm();
              setFieldValue('email', values.email, false);
              setFieldTouched('email', true, false)
              setFieldError('email', err.message);
              setSubmitting(false);
            }
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Please enter a valid email.")
              .required("Email is required."),
            password: Yup.string().required("Password is required.")
          })}
        >
        {props => {
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleChange,
            handleBlur,
            handleSubmit
          } = props;

          return (
          <form
            data-testid="login-form"
            onSubmit={handleSubmit}
            className="form"
          >
            <TextField
              name="email"
              className="text-field"
              variant="outlined"
              placeholder="Email"
              value={values.email}
              onBlur={handleBlur}
              onChange={handleChange}
              error={Boolean(errors.email && touched.email)}
              helperText={touched.email ? errors.email : ''}
              InputProps={{
                classes: {
                  input: classes.inputFont,
                  notchedOutline:
                    !Boolean(errors.email && touched.email) && values.email
                    ? classes.successfulValidation
                    : '',
                }
              }}
              FormHelperTextProps={{
                classes: {
                  root: classes.inputFont
                }
              }}
            />

            <TextField
              name="password"
              className="text-field"
              variant="outlined"
              placeholder="Password"
              type={showingPassword ? "text" : "password"}
              value={values.password}
              onBlur={handleBlur}
              onChange={handleChange}
              error={Boolean(errors.password && touched.password)}
              helperText={touched.password ? errors.password : ''}
              InputProps={{
                classes: {
                  input: classes.inputFont,
                  notchedOutline:
                    !Boolean(errors.password && touched.password) && values.password
                    ? classes.successfulValidation
                    : '',
                },
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={() => setShowingPassword(!showingPassword)}
                      edge="end"
                    >
                      {
                        showingPassword ? <VisibilityIcon /> : <VisibilityOffIcon/>
                      } 
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              FormHelperTextProps={{
                classes: {
                  root: classes.inputFont
                }
              }}
            />
            <div className="forgot-password">
              <Typography className="text forgot-password-text" >
                {"Forgot password? "}
                <Link href={ROUTES.requestResetPassword.path}>
                  Reset.
                </Link>
              </Typography>
              <MuiThemeProvider theme={buttonTheme}>
                <Button
                  data-testid="login-button"
                  className="button"
                  color="primary"
                  variant={
                    !Boolean(errors.email || errors.password)
                    && values.email
                    && values.password
                    ? "contained"
                    : "outlined"
                  }
                  type="submit"
                  disabled={isSubmitting}
                >
                  Login
                </Button>
              </MuiThemeProvider>
            </div>
          </form>
          );
        }}
        </Formik>
      </FormCardLayout>
    </div>
  )
}

export default Login;
