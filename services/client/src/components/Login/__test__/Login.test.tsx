import * as React from 'react';
import { cleanup, render, act, fireEvent, wait } from "@testing-library/react";

import LoginForm from '../Login';

afterEach(cleanup);

const mockAccount = {
  email: 'myemail@abc.com',
  password: 'mypassword'
}
const mockProps = {
  handleSubmitLoginForm: jest.fn(),
  isAuthenticated: jest.fn(),
}

it('render correctly', () => {
  const { getByText } = render(<LoginForm {...mockProps} />);
  expect(getByText('Need help logging in?')).toHaveClass('text');
});

it('handle change text field properly', () => {
  const { getByPlaceholderText } = render(<LoginForm {...mockProps} />);
  const emailInput: any = getByPlaceholderText('Email');
  const passwordInput: any = getByPlaceholderText('Password');
  act(() => {
    fireEvent.change(getByPlaceholderText('Email'), {
      target: { value: mockAccount.email },
    })
    fireEvent.change(getByPlaceholderText('Password'), {
      target: { value: mockAccount.password },
    })
  })

  expect(emailInput.value).toBe(mockAccount.email);
  expect(passwordInput.value).toBe(mockAccount.password);
})

it('submit account object correctly', async () => {
  const mockSubmit = jest.fn();
  const { getByPlaceholderText, getByTestId } = render(
    <LoginForm
      handleSubmitLoginForm={mockSubmit}
      isAuthenticated={jest.fn()}
    />
  );
  await wait(() => {
    fireEvent.change(getByPlaceholderText('Email'), {
      target: { value: mockAccount.email },
    });
    fireEvent.change(getByPlaceholderText('Password'), {
      target: { value: mockAccount.password },
    });
    fireEvent.click(getByTestId('login-button'));
  })

  expect(mockSubmit).toBeCalled();
  expect(mockSubmit).toBeCalledWith(mockAccount);
})

it('enter invalid email/password show error properly', async () => {
  const mockSubmit = jest.fn();
  const { getByPlaceholderText, getByTestId } = render(
    <LoginForm
      handleSubmitLoginForm={mockSubmit}
      isAuthenticated={jest.fn()}
    />
  );
  await wait(() => {
    // set email field to a text without giving correct form
    fireEvent.change(getByPlaceholderText('Email'), {
      target: { value: '#' },
    });
    // leaving password field blank
    fireEvent.click(getByTestId('login-button'));
  })

  // 'aria-invalid' specifies the failed CSS class of Material UI
  expect(getByPlaceholderText('Email')).toHaveAttribute('aria-invalid', 'true');
  expect(getByPlaceholderText('Password')).toHaveAttribute('aria-invalid', 'true');
})

it('rendering matches snapshot', () => {
  const { asFragment } = render(<LoginForm {...mockProps} />);
  expect(asFragment()).toMatchSnapshot();
});
