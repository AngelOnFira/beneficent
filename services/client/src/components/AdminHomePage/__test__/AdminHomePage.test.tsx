import * as React from 'react';
import { cleanup, render, act, fireEvent } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import AdminHomePage from '../AdminHomePage';

afterEach(cleanup);

const mockProps = {
  handleLogout: jest.fn(),
  isAuthenticated: jest.fn(),
  accessToken: "token",
  validRefresh: () => true,
}

it('render correctly', () => {
  mockProps.isAuthenticated.mockReturnValue(true);
  const { getByTestId } = render(
    <BrowserRouter>
      <AdminHomePage {...mockProps} />
    </BrowserRouter>
  );
  expect(getByTestId('dashboard-selection')).toHaveClass('Mui-selected');
});

describe('handle change nav selections', () => {
  mockProps.isAuthenticated.mockReturnValue(true);
  it('cases-and-applications-selected', () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <AdminHomePage {...mockProps} />
      </BrowserRouter>
    );

    act(() => {
      fireEvent.click(getByTestId('cases-and-applications-selection'));
    })

    expect(getByTestId('dashboard-selection')).not.toHaveClass('Mui-selected');
    expect(getByTestId('cases-and-applications-selection')).toHaveClass('Mui-selected');
  });

  it('my-clients-selected', () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <AdminHomePage {...mockProps} />
      </BrowserRouter>
    );

    act(() => {
      fireEvent.click(getByTestId('my-clients-selection'));
    })

    expect(getByTestId('cases-and-applications-selection')).not.toHaveClass('Mui-selected');
    expect(getByTestId('my-clients-selection')).toHaveClass('Mui-selected');
  });

  it('account-manager-selected', () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <AdminHomePage {...mockProps} />
      </BrowserRouter>
    );

    act(() => {
      fireEvent.click(getByTestId('account-manager-selection'));
    })

    expect(getByTestId('my-clients-selection')).not.toHaveClass('Mui-selected');
    expect(getByTestId('account-manager-selection')).toHaveClass('Mui-selected');
  });
});

it('redirect-successfully', () => {
  mockProps.isAuthenticated.mockReturnValue(false);
  const { queryByTestId } = render(
    <BrowserRouter>
      <AdminHomePage {...mockProps} />
    </BrowserRouter>
  );
  expect(queryByTestId('dashboard-selection')).not.toBeInTheDocument();
});

it('rendering matches snapshot', () => {
  const { asFragment } = render(
    <BrowserRouter>
      <AdminHomePage {...mockProps} />
    </BrowserRouter>
  );
  expect(asFragment()).toMatchSnapshot();
});

