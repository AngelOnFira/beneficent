import React, { useState } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';

import SignUp from './components/SignUp/SignUp';
import Login from './components/Login/Login';
import PasswordReset from './components/PasswordReset/PasswordReset';
import PasswordResetRequest from './components/PasswordResetRequest/PasswordResetRequest';
import AdminHomePage from './components/AdminHomePage/AdminHomePage';

// TODO Change this to specific Account settings page for according user types 
import ROUTES from './routes';
import axios from 'axios';


type Account = {
  email: string,
  password: string,
}

/**
 * The main component of the application
 * ** It contains information about user's authentication
 */
const App = () => {
  /**
   * The current browser's access token
   */
  const [accessToken, setAccessToken] = useState('');

  /**
   * The history controlling the page routes
   */
  const history = useHistory();

  /**
   * Check if the current browser is authenticated
   */
  const isAuthenticated = () => {
    console.log(accessToken);
    if (accessToken || validRefresh()) {
      return true;
    }
    return false;
  };

  /**
   * Check authentication of every refresh of the page and reset the token
   */
  const validRefresh = () => {
    const token = window.localStorage.getItem("refreshToken");
    if (token) {
      axios
        .post(`${process.env.REACT_APP_USERS_SERVICE_URL}/auth/refresh`, {
          refresh_token: token
        })
        .then(res => {
          setAccessToken(res.data.access_token);
          window.localStorage.setItem("refreshToken", res.data.refresh_token);
          return true;
        })
        .catch(err => {
          return false;
        });
    }
    return false;
  };

  /**
   * Validate the account to server to get login token 
   * @param account an Object contains 'email' and 'password' properties
   */
  const handleSubmitLoginForm = async (account: Account) => {
    try {
      const url = `${process.env.REACT_APP_USERS_SERVICE_URL}/auth/login`;
      const response = await axios.post(url, account);  
      setAccessToken(response.data.access_token);
      window.localStorage.setItem("refreshToken", response.data.refresh_token);
    } catch (err) {
      throw new Error("Incorrect email or password. Please try again.");
    }
  }

  /**
   * Logout the current user by remove access and
   * refresh token then redirect to login page
   */
  const handleLogout = () => {
    window.localStorage.removeItem("refreshToken");
    setAccessToken('');
    history.push(ROUTES.login.path);
  }

  const handleSubmitSignUpForm = async (values: Object) => {
    
  }

  return (
    <Switch>
      <Route
        exact={ROUTES.home.exact}
        path={ROUTES.home.path}
        render={() => (
          <AdminHomePage
            validRefresh={validRefresh}
            accessToken={accessToken}
            handleLogout={ handleLogout }
            isAuthenticated={ isAuthenticated }
          />
        )}
      />
      <Route
        exact={ROUTES.login.exact}
        path={ROUTES.login.path}
        render={() => (
          <Login
            handleSubmitLoginForm={ handleSubmitLoginForm }
            isAuthenticated={ isAuthenticated }
          />
        )}
      />

      <Route
        exact={ROUTES.resetPassword.exact}
        path={ROUTES.resetPassword.path}
        render={() => (
          <PasswordReset />
        )}
      />

      <Route
        exact={ROUTES.requestResetPassword.exact}
        path={ROUTES.requestResetPassword.path}
        render={() => (
          <PasswordResetRequest />
        )}
      />

      <Route
        exact={ROUTES.signup.exact}
        path={ROUTES.signup.path}
        render={() => (
          <SignUp
            handleSubmitSignUpForm={ handleSubmitSignUpForm }
          />
        )}
      />

      <Route
        exact={ROUTES.contact.exact}
        path={ROUTES.contact.path}
        render={() => (
          <div>Contact us</div> 
        )}
      />
    </Switch>
  );
}

export default App;