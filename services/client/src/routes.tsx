export default {
  home: {
    path: '/app',
    exact: false
  },
  login: {
    path: '/login',
    exact: false 
  },
  signup: {
    path: '/signup',
    exact: false 
  },
  resetPassword: {
    path: '/reset-password/:token',
    exact: false
  },
  requestResetPassword: {
    path: '/reset-request',
    exact: true
  },

  contact: {
    path: '/contact',
    exact: true
  },

  myClients: {
    path: '/clients',
    exact: false,
  },

  accountSettings: {
    path: '/account',
    exact: false,
  },

  accountManager: {
    path: '/manage-accounts',
    exact: false,
  },

  casesApplications: {
    path: '/cases-applications',
    exact: false,
  }
}