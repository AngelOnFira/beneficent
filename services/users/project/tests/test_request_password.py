# services/users/project/tests/test_auth.py

import json


def test_reset_request(test_app, test_database, add_user):
    email = "test@test.com"
    add_user("test", email, "test")
    client = test_app.test_client()
    resp = client.post(
        "/reset-password/reset-request",
        data=json.dumps({"email": email}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert f"Password reset request email was sent to {email}" in data["message"]


def test_reset_request_invalid_email(test_app, test_database, add_user):
    email = "invalidtest@test.com"
    client = test_app.test_client()
    resp = client.post(
        "/reset-password/reset-request",
        data=json.dumps({"email": email}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert resp.content_type == "application/json"
    assert f"User with email {email} does not exist" in data["message"]


def test_reset_password(test_app, test_database, add_user):
    email = "test@test.com"
    password = "newPassword123"
    user = add_user("test", email, "test")
    token = user.get_reset_token()

    client = test_app.test_client()
    resp = client.post(
        "/reset-password/reset-password",
        data=json.dumps({"email": email, "password": password}),
        headers={"Content-Type": "application/json", "Authorization": token},
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert "Password successfully reset" in data["message"]


def test_reset_password_invalid_email(test_app, test_database, add_user):
    email = "test@test.com"
    inavlid_email = "InvalidTest@test.com"
    password = "newPassword123"
    user = add_user("test", email, "test")
    token = user.get_reset_token()

    client = test_app.test_client()
    resp = client.post(
        "/reset-password/reset-password",
        data=json.dumps({"email": inavlid_email, "password": password}),
        headers={"Content-Type": "application/json", "Authorization": token},
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "User does not exist" in data["message"]


def test_reset_password_invalid_token(test_app, test_database, add_user):
    email = "test@test.com"
    password = "newPassword123"
    user = add_user("test", email, "test")
    user.get_reset_token()
    invalid_token = "abcd1234"

    client = test_app.test_client()
    resp = client.post(
        "/reset-password/reset-password",
        data=json.dumps({"email": email, "password": password}),
        headers={"Content-Type": "application/json", "Authorization": invalid_token},
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "That is an invalid or expired token" in data["message"]
