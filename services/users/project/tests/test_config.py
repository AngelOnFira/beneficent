# services/users/project/tests/test_config.py


import os


def test_development_config(test_app):
    test_app.config.from_object("project.config.DevelopmentConfig")
    assert test_app.config["SECRET_KEY"] == "my_precious"
    assert not test_app.config["TESTING"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL")
    assert test_app.config["BCRYPT_LOG_ROUNDS"] == 4
    assert test_app.config["ACCESS_TOKEN_EXPIRATION"] == 900  # new
    assert test_app.config["REFRESH_TOKEN_EXPIRATION"] == 2592000  # new
    assert test_app.config["MAIL_SERVER"] == "smtp.gmail.com"
    assert test_app.config["MAIL_PORT"] == 465
    assert test_app.config["MAIL_USE_TLS"] == False
    assert test_app.config["MAIL_USE_SSL"] == True
    assert test_app.config["MAIL_USERNAME"] == os.environ.get("EMAIL_USER")
    assert test_app.config["MAIL_PASSWORD"] == os.environ.get("EMAIL_PASS")
    assert test_app.config["ALLOWED_EXTENSIONS"] == {"png", "jpg", "jpeg", "gif"}
    assert test_app.config["USER_IMAGES_FOLDER"] == "resources/user_images"


def test_testing_config(test_app):
    test_app.config.from_object("project.config.TestingConfig")
    assert test_app.config["SECRET_KEY"] == "my_precious"
    assert test_app.config["TESTING"]
    assert not test_app.config["PRESERVE_CONTEXT_ON_EXCEPTION"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get(
        "DATABASE_TEST_URL"
    )
    assert test_app.config["BCRYPT_LOG_ROUNDS"] == 4
    assert test_app.config["ACCESS_TOKEN_EXPIRATION"] == 3  # new
    assert test_app.config["REFRESH_TOKEN_EXPIRATION"] == 3  # new
    assert test_app.config["MAIL_SERVER"] == "smtp.gmail.com"
    assert test_app.config["MAIL_PORT"] == 465
    assert test_app.config["MAIL_USE_TLS"] == False
    assert test_app.config["MAIL_USE_SSL"] == True
    assert test_app.config["MAIL_USERNAME"] == os.environ.get("EMAIL_USER")
    assert test_app.config["MAIL_PASSWORD"] == os.environ.get("EMAIL_PASS")
    assert test_app.config["ALLOWED_EXTENSIONS"] == {"png", "jpg", "jpeg", "gif"}
    assert test_app.config["USER_IMAGES_FOLDER"] == "resources/user_images"


def test_production_config(test_app):
    test_app.config.from_object("project.config.ProductionConfig")
    assert test_app.config["SECRET_KEY"] == os.getenv("SECRET_KEY", "my_precious")
    assert not test_app.config["TESTING"]
    assert test_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL")
    assert test_app.config["BCRYPT_LOG_ROUNDS"] == 13
    assert test_app.config["ACCESS_TOKEN_EXPIRATION"] == 900  # new
    assert test_app.config["REFRESH_TOKEN_EXPIRATION"] == 2592000  # new
    assert test_app.config["MAIL_SERVER"] == "smtp.gmail.com"
    assert test_app.config["MAIL_PORT"] == 465
    assert test_app.config["MAIL_USE_TLS"] == False
    assert test_app.config["MAIL_USE_SSL"] == True
    assert test_app.config["MAIL_USERNAME"] == os.environ.get("EMAIL_USER")
    assert test_app.config["MAIL_PASSWORD"] == os.environ.get("EMAIL_PASS")
    assert test_app.config["ALLOWED_EXTENSIONS"] == {"png", "jpg", "jpeg", "gif"}
    assert test_app.config["USER_IMAGES_FOLDER"] == "resources/user_images"
