import json

import pytest
from flask import current_app

from project.api.users.crud import add_application


def test_user_regular_application(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/apps/apply",
        data=json.dumps(
            {
                "first_name": "Elias",
                "last_name": "Hawa",
                "email": "elias.hawa2468@gmail.com",
                "phone_number": "6132622832",
                "address": "123 Sesame Street",
                "city": "Ottawa",
                "province": "Ontario",
                "sex": "Male",
                "date_of_birth": "06/11/2002",
                "marital_status": "Single",
                "citizenship": "Canadian",
                "employement_status": "Employed Full Time",
                "loan_amnt_requested": 1000,
                "loan_type": "Other",
                "debt_circumstances": "Bought way too many watermelons",
                "guarantor": True,
                "recommendation_info": "Fellow watermelons enthusiasts recommended I come here",
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert resp.content_type == "application/json"
    assert "Elias" in data["first_name"]
    assert "Hawa" in data["last_name"]
    assert "elias.hawa2468@gmail.com" in data["email"]
    assert "6132622832" in data["phone_number"]
    assert "123 Sesame Street" in data["address"]
    assert "Ottawa" in data["city"]
    assert "Ontario" in data["province"]
    assert "Male" in data["sex"]
    assert "06/11/2002" in data["date_of_birth"]
    assert "Single" in data["marital_status"]
    assert "Canadian" in data["citizenship"]
    assert "Employed Full Time" in data["employement_status"]
    assert data["loan_amnt_requested"] == 1000
    assert "Other" in data["loan_type"]
    assert "Bought way too many watermelons" in data["debt_circumstances"]
    assert data["guarantor"] == True
    assert (
        "Fellow watermelons enthusiasts recommended I come here"
        in data["recommendation_info"]
    )


def test_user_registration_duplicate_email(test_app):
    info = {
        "first_name": "Jimmy",
        "last_name": "John",
        "email": "Jimmy@John.com",
        "phone_number": "1234567890",
        "address": "22 Wallaby Way",
        "city": "Ottawa",
        "province": "Ontario",
        "sex": "Other",
        "date_of_birth": "11/09/1998",
        "marital_status": "Single",
        "citizenship": "Canadian",
        "employement_status": "Employed Full Time",
        "loan_amnt_requested": 2000,
        "loan_type": "Emergency",
        "debt_circumstances": "Layed off from job, ineligibal to receive EI",
        "guarantor": False,
        "recommendation_info": "A friend of mine recommended me here",
    }
    add_application(info)
    client = test_app.test_client()
    resp = client.post(
        "/apps/apply",
        data=json.dumps(
            {
                "first_name": "Elias",
                "last_name": "Hawa",
                "email": "Jimmy@John.com",
                "phone_number": "6132622832",
                "address": "123 Sesame Street",
                "city": "Ottawa",
                "province": "Ontario",
                "sex": "Male",
                "date_of_birth": "06/11/2002",
                "marital_status": "Single",
                "citizenship": "Canadian",
                "employement_status": "Employed Full Time",
                "loan_amnt_requested": 1000,
                "loan_type": "Other",
                "debt_circumstances": "Bought way too many watermelons",
                "guarantor": True,
                "recommendation_info": "Fellow watermelons enthusiasts recommended I come here",
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert "Sorry, an application is already open with that email" in data["message"]


def test_missing_input(test_app):
    client = test_app.test_client()
    resp = client.post(
        "/apps/apply",
        data=json.dumps(
            {
                "first_name": "Elias",
                "last_name": "Hawa",
                "email": "elias.hawa2468@gmail.com",
                "phone_number": "6132622832",
                "address": "123 Sesame Street",
                "city": "Ottawa",
                "province": "Ontario",
                "sex": "Male",
                "date_of_birth": "06/11/2002",
                "marital_status": "Single",
                "citizenship": "Canadian",
                "employement_status": "Employed Full Time",
                "loan_amnt_requested": 1000,
                "loan_type": "Other",
                "guarantor": True,
                "recommendation_info": "Fellow watermelons enthusiasts recommended I come here",
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert resp.content_type == "application/json"
    assert (
        "'debt_circumstances' is a required property"
        in data["errors"]["debt_circumstances"]
    )
    assert "Input payload validation failed" in data["message"]
