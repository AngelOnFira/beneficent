# services/users.project/tests/test_users.py


import json

import pytest

from project import bcrypt
from project.api.users.crud import get_user_by_id
from project.api.users.models import User


def test_add_user(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/users",
        data=json.dumps(
            {
                "username": "michael",
                "email": "michael@testdriven.io",
                "password": "greaterthaneight",
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert "michael@testdriven.io was added!" in data["message"]


def test_add_user_invalid_json(test_app, test_database):
    client = test_app.test_client()
    resp = client.post("/users", data=json.dumps({}), content_type="application/json",)
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_invalid_json_keys(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/users",
        data=json.dumps({"email": "john@testdriven.io"}),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_duplicate_email(test_app, test_database):
    client = test_app.test_client()
    client.post(
        "/users",
        data=json.dumps(
            {
                "username": "michael",
                "email": "michael@testdriven.io",
                "password": "greaterthaneight",
            }
        ),
        content_type="application/json",
    )
    resp = client.post(
        "/users",
        data=json.dumps(
            {
                "username": "michael",
                "email": "michael@testdriven.io",
                "password": "greaterthaneight",
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Sorry. That email already exists." in data["message"]


def test_single_user(test_app, test_database, add_user):
    user = add_user("jeffrey", "jeffrey@testdriven.io", "greaterthaneight")
    client = test_app.test_client()
    resp = client.get(f"/users/{user.id}")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert "jeffrey" in data["username"]
    assert "jeffrey@testdriven.io" in data["email"]
    assert "password" not in data


def test_single_user_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.get("/users/999")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User 999 does not exist" in data["message"]


def test_all_users(test_app, test_database, add_user):
    test_database.session.query(User).delete()
    add_user("michael", "michael@mherman.org", "greaterthaneight")
    add_user("fletcher", "fletcher@notreal.com", "greaterthaneight")
    client = test_app.test_client()
    resp = client.get("/users")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert "michael" in data[0]["username"]
    assert "michael@mherman.org" in data[0]["email"]
    assert "fletcher" in data[1]["username"]
    assert "fletcher@notreal.com" in data[1]["email"]
    assert "password" not in data[0]
    assert "password" not in data[1]


def test_remove_user(test_app, test_database, add_user):
    test_database.session.query(User).delete()
    user = add_user("user-to-be-removed", "remove-me@testdriven.io", "greaterthaneight")
    client = test_app.test_client()
    resp_one = client.get("/users")
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert len(data) == 1
    resp_two = client.delete(f"/users/{user.id}")
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert "remove-me@testdriven.io was removed!" in data["message"]
    resp_three = client.get("/users")
    data = json.loads(resp_three.data.decode())
    assert resp_three.status_code == 200
    assert len(data) == 0


def test_remove_user_incorrect_id(test_app, test_database):
    client = test_app.test_client()
    resp = client.delete("/users/999")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User 999 does not exist" in data["message"]


def test_update_user(test_app, test_database, add_user):
    user = add_user("user-to-be-updated", "update-me@testdriven.io", "greaterthaneight")
    client = test_app.test_client()
    resp_one = client.put(
        f"/users/{user.id}",
        data=json.dumps({"username": "me", "email": "me@testdriven.io"}),
        content_type="application/json",
    )
    data = json.loads(resp_one.data.decode())
    assert resp_one.status_code == 200
    assert f"{user.id} was updated!" in data["message"]
    resp_two = client.get(f"/users/{user.id}")
    data = json.loads(resp_two.data.decode())
    assert resp_two.status_code == 200
    assert "me" in data["username"]
    assert "me@testdriven.io" in data["email"]


@pytest.mark.parametrize(
    "user_id, payload, status_code, message",
    [
        [1, {}, 400, "Input payload validation failed"],
        [1, {"email": "me@testdriven.io"}, 400, "Input payload validation failed"],
        [
            999,
            {"username": "me", "email": "me@testdriven.io"},
            404,
            "User 999 does not exist",
        ],
    ],
)
def test_update_user_invalid(
    test_app, test_database, user_id, payload, status_code, message
):
    client = test_app.test_client()
    resp = client.put(
        f"/users/{user_id}", data=json.dumps(payload), content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == status_code
    assert message in data["message"]


def test_update_user_with_passord(test_app, test_database, add_user):
    password_one = "greaterthaneight"
    password_two = "somethingdifferent"

    user = add_user("user-to-be-updated", "update-me@testdriven.io", password_one)
    assert bcrypt.check_password_hash(user.password, password_one)

    client = test_app.test_client()
    resp = client.put(
        f"/users/{user.id}",
        data=json.dumps(
            {"username": "me", "email": "me@testdriven.io", "password": password_two}
        ),
        content_type="application/json",
    )
    assert resp.status_code == 200

    user = get_user_by_id(user.id)
    assert bcrypt.check_password_hash(user.password, password_one)
    assert not bcrypt.check_password_hash(user.password, password_two)


def test_update_user_profile(test_app, test_database, add_user):
    user = add_user("update_me", "update_me@testdriven.io", "password")

    client = test_app.test_client()
    resp = client.put(
        f"/users-profile/{user.id}",
        data=json.dumps(
            {
                "first_name": "aashna",
                "last_name": "insertlastname",
                "address": "221b baker street",
                "phone_number": "613200300",
                "country": "Canada",
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert f"{user.id} was updated!" in data["message"]

    user_result = get_user_by_id(user.id)
    assert user_result.first_name == "aashna"
    assert user_result.last_name == "insertlastname"
    assert user_result.address == "221b baker street"
    assert user_result.phone_number == "613200300"
    assert user_result.country == "Canada"


def test_update_user_profile_invalid_user(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.put(
        "/users-profile/100000000",
        data=json.dumps(
            {
                "first_name": "aashna",
                "last_name": "insertlastname",
                "address": "221b baker street",
                "phone_number": "613200300",
                "country": "Canada",
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert "User 100000000 does not exist" in data["message"]


def test_update_user_preferences(test_app, test_database, add_user):
    user = add_user("update_me_again", "update_me_again@testdriven.io", "password")

    client = test_app.test_client()
    resp = client.put(
        f"/users-preferences/{user.id}",
        data=json.dumps(
            {"email": "aashna@newemail.com", "password": "newsecretpassword"}
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert (
        f"{user.id}'s preferences were updated and an email was sent to {user.email}!"
        in data["message"]
    )

    user_result = get_user_by_id(user.id)
    assert user_result.email == "aashna@newemail.com"
    assert bcrypt.check_password_hash(user.password, "newsecretpassword")


def test_update_user_preferences_invalid_user(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.put(
        "/users-preferences/100000000",
        data=json.dumps(
            {"email": "aashna@newemail.com", "password": "newsecretpassword"}
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert "User 100000000 does not exist" in data["message"]
