# services/users/project/api/ping.py
# application API

from flask import request
from flask_restx import Namespace, Resource, fields

from project.api.users.crud import add_application, get_application_by_email
from project.api.users.models import Application

application_namespace = Namespace("apps")

application = application_namespace.model(
    "Application",
    {
        "first_name": fields.String(required=True),
        "last_name": fields.String(required=True),
        "email": fields.String(required=True),
        "phone_number": fields.String(required=True),
        "address": fields.String(required=True),
        "city": fields.String(required=True),
        "province": fields.String(required=True),
        "sex": fields.String(required=True),
        "date_of_birth": fields.String(required=True),
        "marital_status": fields.String(required=True),
        "citizenship": fields.String(required=True),
        "employement_status": fields.String(required=True),
        "loan_amnt_requested": fields.Integer(required=True),
        "loan_type": fields.String(required=True),
        "debt_circumstances": fields.String(required=True),
        "guarantor": fields.Boolean(required=True),
        "recommendation_info": fields.String(required=True),
    },
)


class Application(Resource):
    @application_namespace.expect(application, validate=True)
    @application_namespace.marshal_with(application)
    @application_namespace.response(201, "Success")
    @application_namespace.response(
        400, "Sorry, an application is already open with that email"
    )
    def post(self):
        post_data = request.get_json()
        info = {}
        info_to_get = [
            "first_name",
            "last_name",
            "email",
            "phone_number",
            "address",
            "city",
            "province",
            "sex",
            "date_of_birth",
            "marital_status",
            "citizenship",
            "employement_status",
            "loan_amnt_requested",
            "loan_type",
            "debt_circumstances",
            "guarantor",
            "recommendation_info",
        ]

        for item in info_to_get:
            info[item] = post_data.get(item)

        new_app = get_application_by_email(info["email"])
        if new_app:
            application_namespace.abort(
                400, "Sorry, an application is already open with that email"
            )
        new_app = add_application(info)
        return new_app, 201


application_namespace.add_resource(Application, "/apply")
