# services/users/project/api/users/views.py


import os

from flask import request
from flask_restx import Resource, fields, Namespace
from werkzeug.datastructures import FileStorage
from project import mail
from flask_mail import Message

from project.api.users.crud import (
    get_all_users,
    get_user_by_email,
    add_user,
    get_user_by_id,
    get_user_image,
    update_user,
    delete_user,
    update_user_profile_settings,
    update_user_preferences,
    update_user_image,
)


users_namespace = Namespace("users")

user = users_namespace.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)

user_post = users_namespace.inherit(
    "User post", user, {"password": fields.String(required=True)}
)

user_profile = users_namespace.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "first_name": fields.String(required=True),
        "last_name": fields.String(required=True),
        "address": fields.String(required=True),
        "phone_number": fields.String(required=True),
        "country": fields.String(required=True),
    },
)

user_preferences = users_namespace.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "email": fields.String(required=True),
        "password": fields.String(required=True),
    },
)

upload_parser = users_namespace.parser()
upload_parser.add_argument("image", location="files", type=FileStorage, required=True)


class UsersList(Resource):
    @users_namespace.marshal_with(user, as_list=True)
    def get(self):
        """Returns all users."""
        return get_all_users(), 200

    @users_namespace.expect(user_post, validate=True)
    @users_namespace.response(201, "<user_email> was added!")
    @users_namespace.response(400, "Sorry. That email already exists.")
    def post(self):
        """Creates a new user."""
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        password = post_data.get("password")
        response_object = {}

        user = get_user_by_email(email)
        if user:
            response_object["message"] = "Sorry. That email already exists."
            return response_object, 400
        add_user(username, email, password)
        response_object["message"] = f"{email} was added!"
        return response_object, 201


class Users(Resource):
    @users_namespace.marshal_with(user)
    @users_namespace.response(200, "Success")
    @users_namespace.response(404, "User <user_id> does not exist")
    def get(self, user_id):
        """Returns a single user."""
        user = get_user_by_id(user_id)
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        return user, 200

    @users_namespace.expect(user, validate=True)
    @users_namespace.response(200, "<user_is> was updated!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def put(self, user_id):
        """Updates a user."""
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        user = get_user_by_id(user_id)
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        update_user(user, username, email)
        response_object["message"] = f"{user.id} was updated!"
        return response_object, 200

    @users_namespace.response(200, "<user_is> was removed!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def delete(self, user_id):
        """Updates a user."""
        response_object = {}
        user = get_user_by_id(user_id)
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        delete_user(user)
        response_object["message"] = f"{user.email} was removed!"
        return response_object, 200


class UsersProfile(Resource):
    @users_namespace.expect(user_profile, validate=True)
    @users_namespace.response(200, "<user_id>'s profile was updated!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def put(self, user_id):
        """Updates a user's profile"""
        post_data = request.get_json()
        first_name = post_data.get("first_name")
        last_name = post_data.get("last_name")
        address = post_data.get("address")
        phone_number = post_data.get("phone_number")
        country = post_data.get("country")
        response_object = {}

        user = get_user_by_id(user_id)
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        update_user_profile_settings(
            user, first_name, last_name, address, phone_number, country
        )
        response_object["message"] = f"{user.id} was updated!"
        return response_object, 200


class UsersPreferences(Resource):
    @users_namespace.expect(user_preferences, validate=True)
    @users_namespace.response(
        200,
        "<user_id>'s preferences were updated and an email was sent to <user_email>!",
    )
    @users_namespace.response(404, "User <user_id> does not exist")
    def put(self, user_id):
        """Updates a user's preferences"""
        post_data = request.get_json()
        email = post_data.get("email")
        password = post_data.get("password")
        response_object = {}

        user = get_user_by_id(user_id)
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        update_user_preferences(user, email, password)

        msg = Message(
            "Account Preferences Updated",
            sender=os.environ.get("EMAIL_USER"),
            recipients=[email],
        )
        msg.body = """Your account preferences were recently updated on Beneficent.
        If you did not make these changes, please change your password, or
        contact us at contact@beneficent.cc
        """

        try:
            mail.send(msg)
            response_object[
                "message"
            ] = f"{user.id}'s preferences were updated and an email was sent to {email}!"
            return response_object, 200
        except Exception as e:
            print(f"Failed to send an email to {email}: {e}")
            response_object[
                "Message"
            ] = f"{user.id}'s preferences were updated but failed to send an email to {email}! {e}"

        return response_object, 200


class UsersImage(Resource):
    @users_namespace.expect(upload_parser)
    @users_namespace.response(200, "<user_id>'s profile picture was updated!")
    @users_namespace.response(404, "User <user_id> does not exist")
    @users_namespace.response(400, "Invalid file")
    def post(self, user_id):
        """Change/add a user's profile picture"""
        args = upload_parser.parse_args()
        img = args["image"]
        response_object = {}
        user = get_user_by_id(user_id)
        if not user:
            users_namespace.abort(404, f"User {user_id} does not exist")
        resp = update_user_image(user, img)
        if not resp:
            users_namespace.abort(400, "Invalid file")
        response_object["message"] = f"{user.id}'s profile picture was updated"
        return response_object, 200

    @users_namespace.response(200, "Success!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def get(self, user_id):
        """Returns the path to a users profile picture"""
        path = get_user_image(user_id)
        print(path)
        if not path:
            users_namespace.abort(
                404,
                f"User {user_id} does not have a \
                                  profile picture",
            )
        resp = {}
        resp["filepath"] = path
        resp["message"] = "Success!"
        return resp, 200


users_namespace.add_resource(UsersList, "")
users_namespace.add_resource(Users, "/<int:user_id>")
users_namespace.add_resource(UsersProfile, "-profile/<int:user_id>")
users_namespace.add_resource(UsersPreferences, "-preferences/<int:user_id>")
users_namespace.add_resource(UsersImage, "-profile-picture/<int:user_id>")
