# services/users/project/api/users/models.py


import os
import datetime

import jwt
from flask import current_app
from sqlalchemy.sql import func
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from project import db, bcrypt


class User(db.Model):

    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), nullable=False)
    first_name = db.Column(db.String(128), nullable=True)
    last_name = db.Column(db.String(128), nullable=True)
    email = db.Column(db.String(128), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    address = db.Column(db.String(255), nullable=True)
    phone_number = db.Column(db.String(255), nullable=True)
    country = db.Column(db.String(128), nullable=True)
    image_filepath = db.Column(db.String, nullable=True)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, username="", email="", password=""):
        self.username = username
        self.email = email
        self.password = bcrypt.generate_password_hash(
            password, current_app.config.get("BCRYPT_LOG_ROUNDS")
        ).decode()

    def encode_token(self, user_id, token_type):
        if token_type == "access":
            seconds = current_app.config.get("ACCESS_TOKEN_EXPIRATION")
        else:
            seconds = current_app.config.get("REFRESH_TOKEN_EXPIRATION")

        payload = {
            "exp": datetime.datetime.utcnow() + datetime.timedelta(seconds=seconds),
            "iat": datetime.datetime.utcnow(),
            "sub": user_id,
        }
        return jwt.encode(
            payload, current_app.config.get("SECRET_KEY"), algorithm="HS256"
        )

    @staticmethod
    def decode_token(token):
        payload = jwt.decode(token, current_app.config.get("SECRET_KEY"))
        return payload["sub"]

    def get_reset_token(self, expires_sec=1800):
        """Get a token to be used when a user would like to reset their password
        :param expires_sec: How long before the token should expire
        """
        s = Serializer(current_app.config["SECRET_KEY"], expires_sec)
        return s.dumps({"user_id": self.id}).decode("utf-8")

    @staticmethod
    def verify_reset_token(token):
        s = Serializer(current_app.config["SECRET_KEY"])
        try:
            user_id = s.loads(token)["user_id"]
        except:
            return None
        return User.query.get(user_id)


class Application(db.Model):

    __tablename__ = "application"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(128), nullable=False)
    last_name = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False)
    phone_number = db.Column(db.String(10), nullable=False)
    address = db.Column(db.String(50), nullable=False)
    city = db.Column(db.String(36), default="Ottawa", nullable=False)
    province = db.Column(db.String(30), default="Alberta", nullable=False)
    sex = db.Column(db.String(6), nullable=False)
    date_of_birth = db.Column(db.String(10), nullable=False)
    marital_status = db.Column(db.String(16), default="Single", nullable=False)
    citizenship = db.Column(db.String(25), default="Canadian", nullable=False)
    employement_status = db.Column(
        db.String(25), default="Employed Full Time", nullable=False
    )
    loan_amnt_requested = db.Column(db.Integer, nullable=False)
    loan_type = db.Column(db.String(16), nullable=False)
    debt_circumstances = db.Column(db.Text, nullable=False)
    guarantor = db.Column(db.Boolean, nullable=False)
    recommendation_info = db.Column(db.Text)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(
        self,
        first_name="",
        last_name="",
        email="",
        phone_number="",
        address="",
        city="",
        province="",
        sex="",
        date_of_birth="",
        marital_status="",
        citizenship="",
        employement_status="",
        loan_amnt_requested="",
        loan_type="",
        debt_circumstances="",
        guarantor="",
        recommendation_info="",
    ):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone_number = phone_number
        self.address = address
        self.city = city
        self.province = province
        self.sex = sex
        self.date_of_birth = date_of_birth
        self.marital_status = marital_status
        self.citizenship = citizenship
        self.employement_status = employement_status
        self.loan_amnt_requested = loan_amnt_requested
        self.loan_type = loan_type
        self.debt_circumstances = debt_circumstances
        self.guarantor = guarantor
        self.recommendation_info = recommendation_info


if os.getenv("FLASK_ENV") == "development":
    from project import admin
    from project.api.users.admin import UsersAdminView

    admin.add_view(UsersAdminView(User, db.session))
