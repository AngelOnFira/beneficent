# services/users/project/api/users/crud.py

import os
import string
from random import choice
from flask import current_app
from project import db, bcrypt
from project.api.users.models import User, Application
from werkzeug.utils import secure_filename


def get_all_users():
    return User.query.all()


def get_user_by_id(user_id):
    return User.query.filter_by(id=user_id).first()


def get_user_by_email(email):
    return User.query.filter_by(email=email).first()


def get_user_image(user_id):
    user = get_user_by_id(user_id)
    return user.image_filepath


def add_user(username, email, password):
    user = User(username=username, email=email, password=password)
    db.session.add(user)
    db.session.commit()
    return user


def add_user_all_fields(
    username,
    first_name,
    last_name,
    email,
    password,
    address,
    phone_number,
    country,
    image_filepath,
):
    user = User(
        username=username,
        email=email,
        password=password,
        first_name=first_name,
        last_name=last_name,
        address=address,
        phone_number=phone_number,
        country=country,
        image_filepath=image_filepath,
    )
    db.session.add(user)
    db.session.commit()
    return user


def add_application(info):
    new_app = Application(
        first_name=info["first_name"],
        last_name=info["last_name"],
        email=info["email"],
        phone_number=info["phone_number"],
        address=info["address"],
        city=info["city"],
        province=info["province"],
        sex=info["sex"],
        date_of_birth=info["date_of_birth"],
        marital_status=info["marital_status"],
        citizenship=info["citizenship"],
        employement_status=info["employement_status"],
        loan_amnt_requested=info["loan_amnt_requested"],
        loan_type=info["loan_type"],
        debt_circumstances=info["debt_circumstances"],
        guarantor=info["guarantor"],
        recommendation_info=info["recommendation_info"],
    )
    db.session.add(new_app)
    db.session.commit()
    return new_app


def get_application_by_email(email):
    return Application.query.filter_by(email=email).first()


def update_user(user, username, email):
    user.username = username
    user.email = email
    db.session.commit()
    return user


def update_user_profile_settings(
    user, first_name, last_name, address, phone_number, country
):
    user.first_name = first_name
    user.last_name = last_name
    user.address = address
    user.phone_number = phone_number
    user.country = country
    db.session.commit()
    return user


def update_user_password(user, password):
    hashed_password = bcrypt.generate_password_hash(password).decode("utf-8")
    user.password = hashed_password
    db.session.commit()
    return user


def update_user_preferences(user, email, password):
    user.email = email
    db.session.commit()
    update_user_password(user, password)
    return user


def update_user_image(user, image):
    # can't use Path class methods
    filename = randomize_filename(image.filename)
    print(filename)
    if filename == "" or (not allowed_file(filename)):
        return None
    relative_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(
        relative_path,
        current_app.config["USER_IMAGES_FOLDER"],
        secure_filename(filename),
    )
    image.save(path)
    old_path = user.image_filepath
    if os.path.exists(old_path):
        os.remove(old_path)
    user.image_filepath = path
    db.session.commit()
    return user


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[
        1
    ].lower() in current_app.config.get("ALLOWED_EXTENSIONS")


def randomize_filename(filename):
    letters = string.ascii_lowercase
    new_name = "".join(choice(letters) for i in range(10))
    return new_name + "." + filename.rsplit(".", 1)[1].lower()


def delete_user(user):
    db.session.delete(user)
    db.session.commit()
    return user
