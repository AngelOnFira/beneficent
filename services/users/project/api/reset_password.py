import os

from flask import request
from flask_mail import Message
from flask_restx import Namespace, Resource, fields

from project import mail
from project.api.users.crud import get_user_by_email, update_user_password

reset_pwd_namespace = Namespace("reset-password")

reset_request = reset_pwd_namespace.model(
    "User Password Reset Request", {"email": fields.String(required=True)}
)

reset_password = reset_pwd_namespace.model(
    "User",
    {"email": fields.String(required=True), "password": fields.String(required=True)},
)

parser = reset_pwd_namespace.parser()
parser.add_argument("Authorization", location="headers")


class ResetRequest(Resource):
    @reset_pwd_namespace.expect(reset_request)
    @reset_pwd_namespace.response(
        200, "Password reset request email was sent to <email>"
    )
    @reset_pwd_namespace.response(404, "User with email <email> does not exist")
    def post(self):
        """ Validate email address and send email with link to reset password
        :param email: email address the email with be sent to
        """
        post_data = request.get_json()
        email = post_data.get("email")
        user = get_user_by_email(email)
        response_object = {}

        if not user:
            reset_pwd_namespace.abort(404, f"User with email {email} does not exist")
        token = user.get_reset_token()

        link = "http://localhost:3007/reset-password/" + token
        msg = Message(
            "Password Reset Request",
            sender=os.environ.get("EMAIL_USER"),
            recipients=[email],
        )
        msg.body = f"""To reset your password, visit the following link:
{link}

If you did not make this request then simply ignore this email and no changes will be made.
"""
        try:
            mail.send(msg)
            response_object[
                "message"
            ] = f"Password reset request email was sent to {email}"
            return response_object, 200
        except Exception as e:
            bene_email = os.environ.get("EMAIL_USER")
            bene_pass = os.environ.get("EMAIL_PASS")
            response_object[
                "message"
            ] = f"Failed to send the password reset request email to {email}! {bene_email}:{bene_pass} {e}"

        return response_object, 200


class ResetPassword(Resource):
    @reset_pwd_namespace.expect(reset_password, parser)
    @reset_pwd_namespace.response(400, "That is an invalid or expired token")
    @reset_pwd_namespace.response(400, "User does not exist")
    @reset_pwd_namespace.response(200, "Password has been reset")
    def post(self):
        """Validate password reset token sent in the Authorization header and updates password with the new password that was given
        :param email: user's email
        :param password: the new password
        """
        post_data = request.get_json()
        token = request.headers.get("Authorization")
        email = post_data.get("email")
        password = post_data.get("password")
        user = get_user_by_email(email)
        response_object = {}

        if not user:
            reset_pwd_namespace.abort(400, "User does not exist")

        user = user.verify_reset_token(token)

        if not user:
            reset_pwd_namespace.abort(400, "That is an invalid or expired token.")
        else:

            update_user_password(user, password)
            response_object["message"] = "Password successfully reset"
        return response_object, 200


reset_pwd_namespace.add_resource(ResetRequest, "/reset-request")
reset_pwd_namespace.add_resource(ResetPassword, "/reset-password")
