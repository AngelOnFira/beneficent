# services/users/project/api/__init__.py


from flask_restx import Api

from project.api.application import application_namespace
from project.api.auth import auth_namespace
from project.api.ping import ping_namespace
from project.api.reset_password import reset_pwd_namespace
from project.api.users.views import users_namespace

api = Api(version="1.0", title="Users API", doc="/doc/")

api.add_namespace(ping_namespace, path="/ping")
api.add_namespace(users_namespace, path="/users")
api.add_namespace(auth_namespace, path="/auth")
api.add_namespace(application_namespace, path="/apps")
api.add_namespace(reset_pwd_namespace, path="/reset-password")
